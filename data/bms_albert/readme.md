**Column information:**
- `E_rel_global`: [TARGET FOR BMS] Energy vs. bulk metal and most stable ceria(100) surface ('2O') in units of eV (min: -0.343; max: 1.476)
- `ncoord`: Coordination number of metal single atom (Possible Values: 2, 3, 4)
- `mos`: Metal oxidation state (Possible values: 0, 1, 2; LIKELY HIGHLY CORRELATED TO IONIZATION POTENTIAL)
- `IE_N`: Ionization potential for M^0 -> M^N+ in units of eV (Possible values: 0, 8.9587, 18.56 for M-OS: 0, 1 and 2, respectively)
- `d-Ce3_Ce3`: Separation of two Ce3 centers in units of the direct surface Ce-Ce separation [~3.883A] (Possible values: 1 and sqrt(2) for directly neighboring and diagonally separated Ce3 centers; and 3 for M-OS=0/1, where no two Ce3 centers are present in the super cell)
- `Ce3-ONN_shared`: Indicator if two Ce3 centers are directly bridged/connected by a surface oxygen (Possible values: 1 = True, 0 = False)
